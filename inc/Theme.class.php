<?php
/**
 *	@class  		Theme
 * 
 *  @description 	Wordpress Theme Setup Functions
 */

class Theme {
	
	/**
	 * Construct method and variables
	 * 
	 */
	public function __construct() {
		
		add_action( 'after_setup_theme', array( $this, 'on_theme_setup' ) );
	}
	
	/**
	 * Put items that you want to execute when theme is activated
	 * 
	 */
	function on_theme_setup() {
		
		register_nav_menus( array(
			'primary' 	=> __( 'Primary Navigation', 'my-domain' ),
			'footer' 	=> __( 'Footer Navigation', 'my-domain' )
		) );
		
		add_theme_support( 'post-formats', array( 'aside', 'image' ) );
		add_theme_support( 'post-thumbnails' );
		
	}

} new Theme;