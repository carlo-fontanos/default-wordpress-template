<?php get_header(); ?>

	<div class = "inner-page-wrapper">
		<div class = "container content">
			<?php woocommerce_content(); ?>
		</div>
	</div>
	
<?php get_footer(); ?>